package me.jvt.hacking.domain;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

import java.io.PrintStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Set;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class LinkParserHandlerTest {
  @Mock private LinkParser linkParser;
  private final Deque<Page> workQueue = new ArrayDeque<>();

  private Page startPage;
  private Page child0Page;
  private Page child1Page;

  @BeforeEach
  void setup() throws MalformedURLException {
    URL start = new URL("https://start");
    URL child0 = new URL("https://start/child");
    URL child1 = new URL("https://start/child/grandchild");

    startPage = new Page(start);
    child0Page = new Page(child0);
    child1Page = new Page(child1);
  }

  @Nested
  class CrawlAllLinkedPages {
    private LinkParserHandler handler;

    @BeforeEach
    void setup() {
      handler = new LinkParserHandler();
    }

    @Test
    void itCallsDelegate() {
      handler.crawl(startPage, linkParser, workQueue);

      verify(linkParser).parseLinks(startPage);
    }

    @Test
    void itParsesPage() {
      when(linkParser.parseLinks(startPage)).thenReturn(Set.of(child0Page));

      handler.crawl(startPage, linkParser, workQueue);

      verify(linkParser).parseLinks(startPage);
      verifyNoMoreInteractions(linkParser);
    }

    @Test
    void itStoresVisited() {
      handler.crawl(startPage, linkParser, workQueue);

      var actual = handler.getVisited();

      assertThat(actual).containsExactlyInAnyOrder(startPage);
    }

    @Test
    void itAddsChildrenToWorkQueue() {
      when(linkParser.parseLinks(startPage)).thenReturn(Set.of(child0Page));

      handler.crawl(startPage, linkParser, workQueue);

      assertThat(workQueue).containsExactlyInAnyOrder(child0Page);
    }

    @Test
    void itDoesNotRevisitPageIfPreviouslyVisited() {
      handler = new LinkParserHandler(Set.of(startPage));

      handler.crawl(startPage, linkParser, workQueue);

      verifyNoInteractions(linkParser);
    }
  }

  @Nested
  class Reporting {
    @Mock private PrintStream printStream;
    private LinkParserHandler handler;

    @BeforeEach
    void setup() {
      handler = new ReportingHandler(printStream);
    }

    @Test
    void isCalledWhenAPageIsRecursed() {
      when(linkParser.parseLinks(startPage)).thenReturn(Set.of(child0Page));

      handler.crawl(startPage, linkParser, workQueue);

      verify(printStream).println(startPage);
      verify(printStream).println(Set.of(child0Page));
    }

    @Test
    void isCalledIfNoChildren() {
      handler.crawl(startPage, linkParser, workQueue);

      verify(printStream).println(startPage);
      verify(printStream).println(Set.of());
    }

    class ReportingHandler extends LinkParserHandler {

      private final PrintStream printStream;

      public ReportingHandler(PrintStream printStream) {
        super();
        this.printStream = printStream;
      }

      @Override
      public void reportCrawlStatus(Page currentPage, Set<Page> linkedPages) {
        printStream.println(currentPage);
        printStream.println(linkedPages);
      }
    }
  }

  @Nested
  class Filtering {
    private LinkParserHandler handler;
    private Page validHttpsPage;
    private Page validHttpPage;
    private Page ignoredPage;

    @BeforeEach
    void setup() throws MalformedURLException {
      URL validHttps = new URL("https://valid.com/foo/bar");
      URL validHttp = new URL("http://valid.com/foo/bar");
      URL ignored = new URL("https://valid.com.za");
      validHttpsPage = new Page(validHttps);
      validHttpPage = new Page(validHttp);
      ignoredPage = new Page(ignored);

      handler = new FilteringHandler();
    }

    @Test
    void itDoesNotParseLinksOnFilteredPages() {
      when(linkParser.parseLinks(validHttpsPage)).thenReturn(Set.of(validHttpPage));

      handler.crawl(validHttpsPage, linkParser, workQueue);

      verify(linkParser).parseLinks(validHttpsPage);
      verifyNoMoreInteractions(linkParser);
    }

    @Test
    void itDoesNotIncludedIgnoredLinksInVisitedPages() {
      handler.crawl(ignoredPage, linkParser, workQueue);
      var actual = handler.getVisited();

      assertThat(actual).isEmpty();
    }

    class FilteringHandler extends LinkParserHandler {

      @Override
      public boolean shouldCrawl(Page pageToCrawl) {
        return pageToCrawl.url().getHost().equals("valid.com");
      }
    }
  }
}
