package me.jvt.hacking.application;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

import java.io.PrintStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Set;
import me.jvt.hacking.domain.LinkParser;
import me.jvt.hacking.domain.Page;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class RecursiveLinkParserServiceTest {
  @Mock private LinkParser linkParser;

  private RecursiveLinkParserService service;

  @Nested
  class CrawlAllLinkedPages {

    private URL start;
    private URL child0;
    private URL child1;

    @BeforeEach
    void setup() throws MalformedURLException {
      start = new URL("https://start");
      child0 = new URL("https://start/child");
      child1 = new URL("https://start/child/grandchild");
      service = new RecursiveLinkParserService(start, linkParser);
    }

    @Test
    void itCallsDelegate() throws MalformedURLException {
      service.crawlAllLinkedPages();

      verify(linkParser).parseLinks(new Page(start));
    }

    @Test
    void itRecurses() {
      when(linkParser.parseLinks(new Page(start))).thenReturn(Set.of(new Page(child0)));

      service.crawlAllLinkedPages();

      InOrder inOrder = inOrder(linkParser);
      inOrder.verify(linkParser).parseLinks(new Page(start));
      inOrder.verify(linkParser).parseLinks(new Page(child0));
      verifyNoMoreInteractions(linkParser);
    }

    @Test
    void itReturns() {
      when(linkParser.parseLinks(new Page(start))).thenReturn(Set.of(new Page(child0)));
      when(linkParser.parseLinks(new Page(child0))).thenReturn(Set.of(new Page(child1)));

      var actual = service.crawlAllLinkedPages();

      assertThat(actual)
          .containsExactlyInAnyOrder(new Page(start), new Page(child0), new Page(child1));
    }

    @Test
    void itDoesNotRevisitSamePage() throws MalformedURLException {
      URL child2 = new URL("https://start/child");

      when(linkParser.parseLinks(new Page(start))).thenReturn(Set.of(new Page(child0)));
      when(linkParser.parseLinks(new Page(child0))).thenReturn(Set.of(new Page(child1)));
      when(linkParser.parseLinks(new Page(child1)))
          .thenReturn(Set.of(new Page(child1), new Page(child2)));

      service.crawlAllLinkedPages();

      verify(linkParser).parseLinks(new Page(start));
      verify(linkParser).parseLinks(new Page(child0));
      verify(linkParser, times(1)).parseLinks(new Page(child1));
      verify(linkParser).parseLinks(new Page(child2));
    }
  }

  @Nested
  class Reporting {
    @Mock private PrintStream printStream;

    private URL start;
    private URL child0;
    private URL child1;

    @BeforeEach
    void setup() throws MalformedURLException {
      start = new URL("https://start");
      child0 = new URL("https://start/child");
      child1 = new URL("https://start/child/grandchild");
      service = new ReportingService(start, linkParser, printStream);
    }

    @Test
    void isCalledWhenAPageIsRecursed() {
      Page startPage = new Page(start);
      Page childPage0 = new Page(child0);
      when(linkParser.parseLinks(startPage)).thenReturn(Set.of(childPage0));

      service.crawlAllLinkedPages();

      verify(printStream).println(startPage);
      verify(printStream).println(Set.of(childPage0));
    }

    @Test
    void isCalledWithAllPagesOnAPageDespiteIfSeenBefore() {
      Page startPage = new Page(start);
      Page childPage0 = new Page(child0);
      Page childPage1 = new Page(child1);
      when(linkParser.parseLinks(startPage)).thenReturn(Set.of(childPage0));
      when(linkParser.parseLinks(childPage0)).thenReturn(Set.of(childPage0, childPage1));

      service.crawlAllLinkedPages();

      verify(printStream).println(startPage);
      verify(printStream).println(Set.of(childPage0));
      verify(printStream).println(startPage);
      verify(printStream).println(Set.of(childPage0, childPage1));
    }

    @Test
    void isCalledIfNoChildren() {
      service.crawlAllLinkedPages();

      verify(printStream).println(new Page(start));
      verify(printStream).println(Set.of());
    }

    class ReportingService extends RecursiveLinkParserService {

      private final PrintStream printStream;

      public ReportingService(URL startUrl, LinkParser delegate, PrintStream printStream) {
        super(startUrl, delegate);
        this.printStream = printStream;
      }

      @Override
      public void reportCrawlStatus(Page currentPage, Set<Page> linkedPages) {
        printStream.println(currentPage);
        printStream.println(linkedPages);
      }
    }
  }

  @Nested
  class Filtering {
    private URL valid;
    private URL validHttp;

    @BeforeEach
    void setup() throws MalformedURLException {
      valid = new URL("https://valid.com/foo/bar");
      validHttp = new URL("http://valid.com/foo/bar");
      URL ignored = new URL("https://valid.com.za");
      service = new FilteringService(valid, linkParser);

      when(linkParser.parseLinks(new Page(valid))).thenReturn(Set.of(new Page(validHttp)));
      when(linkParser.parseLinks(new Page(validHttp))).thenReturn(Set.of(new Page(ignored)));
    }

    @Test
    void itDoesNotParseLinksOnFilteredPages() {
      service.crawlAllLinkedPages();

      verify(linkParser).parseLinks(new Page(valid));
      verify(linkParser).parseLinks(new Page(validHttp));
      verifyNoMoreInteractions(linkParser);
    }

    @Test
    void itDoesNotIncludedIgnoredLinksInVisitedPages() {
      var actual = service.crawlAllLinkedPages();

      assertThat(actual).containsExactlyInAnyOrder(new Page(valid), new Page(validHttp));
    }

    class FilteringService extends RecursiveLinkParserService {

      public FilteringService(URL startUrl, LinkParser delegate) {
        super(startUrl, delegate);
      }

      @Override
      public boolean shouldCrawl(Page pageToCrawl) {
        return pageToCrawl.url().getHost().equals("valid.com");
      }
    }
  }
}
