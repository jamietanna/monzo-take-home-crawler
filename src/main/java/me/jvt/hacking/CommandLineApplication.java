package me.jvt.hacking;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingDeque;
import me.jvt.hacking.application.JsoupLinkParser;
import me.jvt.hacking.application.QueueProcessor;
import me.jvt.hacking.domain.Page;

public class CommandLineApplication {

  private static final int THREADS = 5;
  private static final int RETRY_COUNT = 10;
  private static final int RETRY_DELAY = 500;

  public static void main(String[] args) {
    if (args.length != 1) {
      System.err.println(
          "A command-line argument that is the URL to start with needs to be provided");
      System.exit(1);
    }

    URL startingUrl = null;
    try {
      startingUrl = new URL(args[0]);
    } catch (MalformedURLException e) {
      System.err.println("Provided URL was malformed" + e.getMessage());
      System.exit(1);
    }

    ExecutorService executorService = Executors.newFixedThreadPool(THREADS);

    Set<Page> visited = ConcurrentHashMap.newKeySet();

    LinkedBlockingDeque<Page> queue = new LinkedBlockingDeque<>();
    queue.add(new Page(startingUrl));
    var linkParser = new JsoupLinkParser();
    for (int i = 0; i < THREADS; i++) {
      executorService.execute(new QueueProcessor(startingUrl, visited, linkParser, queue));
    }

    // wait until the queue is empty - for a prolonged period of time, which likely means we've
    // finished processing
    int retries = 0;
    while (retries <= RETRY_COUNT) {
      if (queue.isEmpty()) {
        retries++;
      } else {
        retries = 0;
      }
      try {
        Thread.sleep(RETRY_DELAY);
      } catch (InterruptedException e) {
        Thread.currentThread().interrupt();
      }
    }

    executorService.shutdownNow();
  }
}
