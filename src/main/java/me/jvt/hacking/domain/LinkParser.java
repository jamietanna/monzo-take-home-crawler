package me.jvt.hacking.domain;

import java.util.Set;

public interface LinkParser {
  /**
   * Parse the provided {@link Page} for any link(s) present on it.
   *
   * @param page the {@link Page} to parse
   * @return the {@link Page}(s) that are linked
   */
  Set<Page> parseLinks(Page page);
}
