package me.jvt.hacking.domain;

import java.util.Collections;
import java.util.Deque;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class LinkParserHandler {
  protected final Set<Page> visited;

  protected LinkParserHandler() {
    this(new HashSet<>());
  }

  protected LinkParserHandler(Set<Page> visited) {
    this.visited = visited;
  }

  /**
   * Traverse links on a given {@link Page}, using the provided {@link LinkParser}, ensuring any new
   * items are added to the internal work queue.
   *
   * @param page the {@link Page} to traverse links for
   * @param linkParser the {@link LinkParser} to parse the {@link Page}'s links
   * @param workQueue the queue of work items left
   */
  public void crawl(Page page, LinkParser linkParser, Deque<Page> workQueue) {
    if (!shouldCrawl(page)) {
      return;
    }

    if (visited.contains(page)) {
      return;
    }

    visited.add(page);

    var children =
        linkParser.parseLinks(page).stream().filter(this::shouldCrawl).collect(Collectors.toSet());
    // report before we proceed, otherwise we're going to get pages reported _very late_
    // compared to when they were crawled
    reportCrawlStatus(page, children);

    if (children.isEmpty()) {
      return;
    }

    for (Page child : children) {
      if (!visited.contains(child)) {
        workQueue.add(child);
      }
    }
  }

  /**
   * Report the status of the page, once it has been crawled.
   *
   * <p>By default, is a no-op.
   *
   * @param currentPage the {@link Page} that has just been crawled
   * @param linkedPages the {@link Page}(s) that are linked to from <code>currentPage</code>
   */
  public void reportCrawlStatus(Page currentPage, Set<Page> linkedPages) {
    // by default, don't do anything
  }

  /**
   * Whether to crawl a given page.
   *
   * <p>Allows adding business logic, such as only requiring HTTPS URLs, ignoring internal domains
   * or only browsing to certain domains.
   *
   * <p>Defaults to requring HTTP and HTTPS URLs.
   *
   * @param pageToCrawl the {@link Page} that would be crawled
   * @return <code>true</code> to crawl
   */
  public boolean shouldCrawl(Page pageToCrawl) {
    return pageToCrawl.url().getProtocol().equals("http")
        || pageToCrawl.url().getProtocol().equals("https");
  }

  /**
   * Get the pages visited by this {@link LinkParserHandler}.
   *
   * <p>Note that this is an immutable copy.
   *
   * @return an immutable copy of the pages visited at the point in time this method is called
   */
  public Set<Page> getVisited() {
    return Collections.unmodifiableSet(visited);
  }
}
