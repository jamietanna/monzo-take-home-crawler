package me.jvt.hacking.domain;

import java.net.URL;
import java.util.Set;

/**
 * An original implementation that allowed building a recursive means to perform link parsing, but
 * not (easily?) parallelisable.
 *
 * @deprecated use {@link LinkParserHandler}s instead
 */
@Deprecated(forRemoval = true)
public abstract class LinkParserService {

  protected URL startUrl;

  /**
   * From a configured <code>startUrl</code>, recursively crawl all resolved {@link Page}s.
   *
   * @return the {@link Page}s that have been resolved
   */
  public abstract Set<Page> crawlAllLinkedPages();

  /**
   * Report the status of the page, once it has been crawled.
   *
   * <p>By default, is a no-op.
   *
   * @param currentPage the {@link Page} that has just been crawled
   * @param linkedPages the {@link Page}(s) that are linked to from <code>currentPage</code>
   */
  public void reportCrawlStatus(Page currentPage, Set<Page> linkedPages) {}

  /**
   * Whether to crawl a given page.
   *
   * <p>Allows adding business logic, such as only requiring HTTPS URLs, ignoring internal domains
   * or only browsing to certain domains.
   *
   * <p>Defaults to requring HTTP and HTTPS URLs.
   *
   * @param pageToCrawl the {@link Page} that would be crawled
   * @return <code>true</code> to crawl
   */
  public boolean shouldCrawl(Page pageToCrawl) {
    return pageToCrawl.url().getProtocol().equals("http")
        || pageToCrawl.url().getProtocol().equals("https");
  }
}
