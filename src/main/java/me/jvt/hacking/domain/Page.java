package me.jvt.hacking.domain;

import java.net.URL;

public record Page(URL url) {}
