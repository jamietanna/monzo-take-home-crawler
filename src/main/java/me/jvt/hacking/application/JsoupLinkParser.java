package me.jvt.hacking.application;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import me.jvt.hacking.domain.LinkParser;
import me.jvt.hacking.domain.Page;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/*
Right now this class is "trivial enough" to leave as an untested case.

We would ideally replace it with an Integration test against a stubbed web server, to validate that it works correctly.
 */
public class JsoupLinkParser implements LinkParser {
  @Override
  public Set<Page> parseLinks(Page page) {
    try {
      Set<Page> pageLinks = new HashSet<>();
      Document document = Jsoup.connect(page.url().toString()).get();
      Elements links = document.select("a[href]");
      for (Element link : links) {
        var href = link.absUrl("href");
        var url = parseUrl(href);
        url.ifPresent(value -> pageLinks.add(new Page(value)));
      }
      return pageLinks;
    } catch (IOException e) {
      return Set.of();
    }
  }

  private static Optional<URL> parseUrl(String href) {
    try {
      var url = new URL(href);
      return Optional.of(url);
    } catch (MalformedURLException ignored) {
      return Optional.empty();
    }
  }
}
