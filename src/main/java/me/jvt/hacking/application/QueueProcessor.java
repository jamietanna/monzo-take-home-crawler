package me.jvt.hacking.application;

import java.net.URL;
import java.util.Set;
import java.util.concurrent.BlockingDeque;
import me.jvt.hacking.domain.LinkParser;
import me.jvt.hacking.domain.Page;

public class QueueProcessor extends MonzoLinkParserHandler implements Runnable {

  private final LinkParser parser;
  private final BlockingDeque<Page> queue;

  public QueueProcessor(
      URL startUrl, Set<Page> visited, LinkParser parser, BlockingDeque<Page> queue) {
    super(startUrl, visited);
    this.parser = parser;
    this.queue = queue;
  }

  @Override
  public void run() {
    while (!(Thread.currentThread().isInterrupted())) {
      try {
        var page = queue.take();
        crawl(page, parser, queue);
      } catch (InterruptedException e) {
        Thread.currentThread().interrupt();
      }
    }

    if (!queue.isEmpty()) {
      // NOTE that I didn't get around to this, but we'd ideally want to process the items in the
      // queue, or place them in a space for the next run, or perform better eventing/logging to
      // make it clear that they need to be processed
      System.err.println("Starting to shut down, still " + queue.size() + " items left");
    }
  }
}
