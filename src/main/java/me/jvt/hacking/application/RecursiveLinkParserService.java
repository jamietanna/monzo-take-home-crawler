package me.jvt.hacking.application;

import java.net.URL;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import me.jvt.hacking.domain.LinkParser;
import me.jvt.hacking.domain.LinkParserService;
import me.jvt.hacking.domain.Page;

public class RecursiveLinkParserService extends LinkParserService {
  private final LinkParser delegate;

  public RecursiveLinkParserService(URL startUrl, LinkParser delegate) {
    this.startUrl = startUrl;
    this.delegate = delegate;
  }

  @Override
  public Set<Page> crawlAllLinkedPages() {
    Set<Page> visited = new HashSet<>();
    recurse(new Page(startUrl), visited);
    return visited;
  }

  /**
   * Recurse through a page and find any linked pages, storing them in the provided <code>
   * visited</code>.
   *
   * @param page the {@link Page} to recurse through
   * @param visited the state that's currently available
   */
  private void recurse(Page page, Set<Page> visited) {
    if (!shouldCrawl(page)) {
      return;
    }
    visited.add(page);

    var children =
        delegate.parseLinks(page).stream().filter(this::shouldCrawl).collect(Collectors.toSet());
    // report before we recurse or return, otherwise we're going to get pages reported _very late_
    // compared to when they were crawled
    reportCrawlStatus(page, children);

    if (children.isEmpty()) {
      return;
    }

    for (Page child : children) {
      if (!visited.contains(child)) {
        recurse(child, visited);
      }
      visited.add(child);
    }
  }
}
