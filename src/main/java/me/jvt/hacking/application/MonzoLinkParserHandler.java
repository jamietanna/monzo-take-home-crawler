package me.jvt.hacking.application;

import java.net.URL;
import java.util.Set;
import java.util.StringJoiner;
import me.jvt.hacking.domain.LinkParserHandler;
import me.jvt.hacking.domain.Page;

/** An implementation of the {@link LinkParserHandler} for the requirements provided. */
public class MonzoLinkParserHandler extends LinkParserHandler {

  private final URL startUrl;

  public MonzoLinkParserHandler(URL startUrl, Set<Page> visited) {
    super(visited);
    this.startUrl = startUrl;
  }

  @Override
  public void reportCrawlStatus(Page currentPage, Set<Page> linkedPages) {
    if (linkedPages.isEmpty()) {
      System.out.println(
          "Page " + currentPage.url().toString() + " did not link to anything on the same domain");
      return;
    }

    StringJoiner joiner = new StringJoiner("\n- ");
    joiner.add("Page " + currentPage.url().toString() + " linked out to:");
    linkedPages.forEach(l -> joiner.add(l.url().toString()));
    System.out.println(joiner);
  }

  @Override
  public boolean shouldCrawl(Page pageToCrawl) {
    return super.shouldCrawl(pageToCrawl) && pageToCrawl.url().getHost().equals(startUrl.getHost());
  }
}
