# Command-Line Web Scraper

A Java command-line application to traverse a domain's pages and their links, as part of Jamie Tanna's solution for the Monzo Software Engineering interview process.

## Usage

To build the JAR, you will need a JDK 17 installation.

Then you can run:

```sh
./gradlew clean build
```

This can then be executed, reading a single line from `stdin`, for instance:

```sh
java -jar build/libs/scraper.jar https://monzo.com
# or
java -jar build/libs/scraper.jar https://www.jvt.me
```

Note that the application may "hang" for several seconds, either during the execution of the parsing, or at the end of processing. This is to ensure that there are no new items to be processed.

If the timeouts need to be increased, or parallelisation needs to be improved, the `CommandLineApplication` class contains configuration options.

## Architecture

The command-line interface can be found in the `me.jvt.hacking.CommandLineApplication` class.

The underlying code can be found in `me.jvt.hacking.domain` which is split into:

- `LinkParser`, an abstraction for implementation-specific parsing of links
- `LinkParserHandler`, for a common implementation of traversing a `Page` and its links, while allowing custom processing, such as a user-provided means to configure whether we `shouldCrawl`, or to add i.e. logging based on each page being visited

In the `me.jvt.hacking.application` package, we have the application-specific implementations:

- `JsoupLinkParser`, which uses the Java library Jsoup to retrieve `<a href>`s on a given URL
- `MonzoLinkParserHandler`, which is the implementation that handles the Monzo requirements - only traverse the linked domain, and output log messages when visiting URLs
- `QueueProcessor`, which is used for parallel processing

### Previous iteration

As previous version, `RecursiveLinkParserService` can be found, as the first version of the architecture + solution. I have since replaced this with a parallelisable version, but left the recursive solution as an example.
